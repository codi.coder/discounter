module Rule
  class GetBonusItem < Base
    private

    def execute_rule(items)
      items.select { |x| !x.discounted? && x.code?(discounted_product_code) }.size.times do
        discounted_item = CartItem.new(discounted_product.name, discounted_product.code, discounted_product.price)
        discounted_item.apply_discount(discounted_price, true)
        items << discounted_item
      end
    end

    def discounted_product
      @discounted_product ||= Product.find(discounted_product_code)
    end
  end
end
