class RuleSet
  def initialize(cart)
    @cart = cart
    @rules = []
  end

  def add_rule(rule)
    rules << rule
  end

  def apply
    rules.each do |rule|
      rule.apply(cart)
    end
  end

  private

  attr_reader :rules
end
