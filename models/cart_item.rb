class CartItem
  attr_reader :name, :code, :price, :discount_applied, :discounted_price, :bonus_item

  def initialize(name, code, price)
    @name = name
    @code = code
    @price = price
    @discounted_price = price
    @discount_applied = discount_applied
    @bonus_item = false
  end

  def discounted?
    discount_applied
  end

  def bonus_item?
    bonus_item
  end

  def code?(check_code)
    code == check_code
  end

  def apply_discount(new_price, bonus_item = false)
    @discounted_price = new_price
    @discount_applied = true
    @bonus_item = bonus_item
  end

  def reset_discount!
    @discounted_price = @price
    @discount_applied = false
  end
end
