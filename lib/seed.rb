class Seed
  def self.setup
    Product.create(name: 'Fruit Tea', code: 'FR', price: 3.11)
    Product.create(name: 'Strawberries', code: 'SR', price: 5.00)
    Product.create(name: 'Coffee', code: 'CF', price: 11.23)
    Product.create(name: 'Apple Juice', code: 'AJ', price: 7.25)

    Rule::DiscountAllOcurrance.create('three_strawberries', %w[SR SR SR], 'SR', 4.5)
    Rule::GetBonusItem.create('free_fruit_tea', %w[FR], 'FR', 0.0)
  end
end
